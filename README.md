Nightly CI
----------

This CI runs once a day.

Add new job
-----------

Edit .gitlab-ci.yml and add a job extending *.scheduled-job*:

```
your-job:
    extends: .scheduled-job
    script:
        - echo "Do your thing!"
```

Setup
-----

Pipelines failures are automatically sent to:
woa-support@op-lists.linaro.org

This is done through:
Settings -> Integration -> Pipeline Status email

Schedule is set up with:
CI/CD -> Schedules -> New Schedule

You can register to mailing list from this link:
https://op-lists.linaro.org/mailman3/lists/woa-support.op-lists.linaro.org/
